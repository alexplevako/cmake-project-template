#include <iostream>

#include <project/first/first.hpp>

#ifndef PROJECT_NAME
#define PROJECT_NAME "template"
#endif

#ifndef DEBUG
#define DEBUG 0
#endif

bool IsDebug()
{
    return DEBUG;
}

int main(int , char const *[])
{
    if (IsDebug())
    {
        std::cout << "DEBUG" << "\n";
    }

    project::first::first();
    
    std::cout << PROJECT_NAME << "\n";
    return 0;
}
